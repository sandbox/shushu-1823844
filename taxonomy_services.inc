<?php
/**
 * @file
 *  Link general views functionalities to services module.
 */


function taxonomy_services_get_vocabularies() {

  return taxonomy_get_vocabularies();
}

/**
 * Check the access permission to a given views.
 *
 * @param view_name
 *   String. The views name.
 * @return
 *   Boolean. TRUE if the user is allowed to load the given view.
 */
function taxonomy_services_get_vocabularies_access() {
  return true;
}
function taxonomy_services_get_tree_access() {
  return true;
}

function taxonomy_services_get_tree($vid, $parent = 0, $max_depth = NULL) {
  $tree = taxonomy_get_tree($vid, $parent, -1, $max_depth);

//  watchdog("mobilesocial debug", "got tree for $vid");

  // Add images
  if (module_exists("taxonomy_image")) {
    foreach ($tree as &$item) {
      $tid = $item->tid;
	watchdog("mobilesocial", "tid = $tid, getting image");
      $item->taxonomy_image = taxonomy_image_get_url($tid);
	watchdog("mobilesocial", taxonomy_image_get_url($tid));
    }
  }

  return $tree;
}
